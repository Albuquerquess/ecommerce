import { Response } from '@b4-org/middleware-express/ExpressTypes';
import { Request } from 'express';
import { container } from 'tsyringe';
import { IIndexItemsRequest } from '../../../types/cart';
import { IndexCartItemsUseCase } from './indexCartItemsUseCase';

class IndexCartItemsController {
    async handle(request: Request, response: Response) {
        const { ecommerce_id: ecommerceId, client_id: clientId } =
            request.query as unknown as IIndexItemsRequest;

        const indexCartItemsUseCase = container.resolve(IndexCartItemsUseCase);

        const { ecommerce, cart } = await indexCartItemsUseCase.execute({
            clientId,
            ecommerceId
        });

        return response.json({
            result: {
                cart
            },
            resultInfo: ecommerce
        });
    }
}

export { IndexCartItemsController };
