import { AppError } from '@b4-org/middleware-express';
import { ICartRepository } from '@modules/ecommerce/repository/ICartRepository';
import { IEcommerceRepository } from '@modules/ecommerce/repository/IEcommerceRepository';
import { inject, injectable } from 'tsyringe';

@injectable()
class IndexCartItemsUseCase {
    constructor(
        @inject('CartRepository')
        private cartRepository: ICartRepository,
        @inject('EcommerceRepository')
        private ecommerceRepository: IEcommerceRepository
    ) {}

    async execute({ ecommerceId, clientId }) {
        const ecommerceExists =
            await this.ecommerceRepository.checkEcommerceExistence(ecommerceId);

        if (!ecommerceExists)
            throw new AppError(
                [
                    {
                        message: 'ecommerce not found',
                        code: 'indexCartItems.ecommerceNotFound'
                    }
                ],
                '404 Not Found' // Ou 400 bad Request?
            );
        const items = await this.cartRepository.indexCartItems(
            ecommerceId,
            clientId
        );

        return items;
    }
}

export { IndexCartItemsUseCase };
