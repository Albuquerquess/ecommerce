import { IEcommerceProductsRepository } from '@modules/ecommerce/repository/IEcommerceProductsRepository';
import { ICartRepository } from '@modules/ecommerce/repository/ICartRepository';
import { IEcommerceRepository } from '@modules/ecommerce/repository/IEcommerceRepository';
import { inject, injectable } from 'tsyringe';
import { AppError } from '@b4-org/middleware-express';

interface IRequestAddItem {
    ecommerceId: number;
    clientId: number;
    planId: number;
    quantity: number;
}
@injectable()
class AddItemUseCase {
    constructor(
        @inject('CartRepository')
        private cartRepository: ICartRepository,
        @inject('EcommerceRepository')
        private ecommerceRepository: IEcommerceRepository
    ) {}

    async execute({
        ecommerceId,
        clientId,
        planId,
        quantity
    }: IRequestAddItem) {
        const ecommerceExists =
            await this.ecommerceRepository.checkEcommerceExistence(ecommerceId);

        if (!ecommerceExists)
            throw new AppError(
                [
                    {
                        message: 'ecommerce not found',
                        code: 'addItemToCart.ecommerceNotFound'
                    }
                ],
                '404 Not Found'
            );

        const ecommerceStatus =
            await this.ecommerceRepository.checkEcommerceStatus(ecommerceId);

        if (!ecommerceStatus)
            throw new AppError(
                [
                    {
                        message: 'ecommerce not found',
                        code: 'addItemToCart.ecommerceNotFound'
                    }
                ],
                '404 Not Found'
            );

        const checkCartExistence = await this.cartRepository.checkCartExistence(
            ecommerceId,
            clientId
        );

        if (!checkCartExistence) {
            await this.cartRepository.createCart(ecommerceId, clientId);
        }
        const cartItemExistance =
            await this.cartRepository.checkCartItemExistance(
                ecommerceId,
                clientId,
                planId
            );

        if (cartItemExistance)
            throw new AppError(
                [
                    {
                        message: 'item already added on cart',
                        code: 'addItemToCart.itemAlreadyAdded'
                    }
                ],
                '409 Conflict'
            );

        const addedItem = await this.cartRepository.addItemToCart(
            planId,
            quantity,
            ecommerceId,
            clientId
        );

        return addedItem;
    }
}

export { AddItemUseCase };
