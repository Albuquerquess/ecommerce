import { Response } from '@b4-org/middleware-express/ExpressTypes';
import { Request } from 'express';
import { container } from 'tsyringe';
import { AddItemUseCase } from './addItemUseCase';

class AddItemController {
    async handle(request: Request, response: Response) {
        const {
            ecommerce_id: ecommerceId,
            client_id: clientId,
            plan_id: planId,
            quantity
        } = request.body;

        const addItemUseCase = container.resolve(AddItemUseCase);

        const addItem = await addItemUseCase.execute({
            ecommerceId,
            clientId,
            planId,
            quantity
        });

        return response.json({
            result: {
                added: addItem
            }
        });
    }
}

export { AddItemController };
