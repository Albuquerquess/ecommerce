import { Request } from 'express';
import { Response } from '@b4-org/middleware-express/ExpressTypes';

import { container } from 'tsyringe';
import { RemoveItemUseCase } from './removeItemUseCase';

class RemoveItemController {
    async handle(request: Request, response: Response) {
        const {
            ecommerce_id: ecommerceId,
            client_id: clientId,
            plan_id: planId
        } = request.body;

        const removeItemUseCase = container.resolve(RemoveItemUseCase);

        const removeItem = await removeItemUseCase.execute({
            ecommerceId,
            clientId,
            planId
        });

        return response.json({
            result: {
                removed: removeItem
            }
        });
    }
}

export { RemoveItemController };
