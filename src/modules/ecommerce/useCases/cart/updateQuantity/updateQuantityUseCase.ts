import { AppError } from '@b4-org/middleware-express';
import { ICartRepository } from '@modules/ecommerce/repository/ICartRepository';
import { IEcommerceRepository } from '@modules/ecommerce/repository/IEcommerceRepository';
import { inject, injectable } from 'tsyringe';
import { IRequestUpdateQuantityItem } from '../../../types/cart.d';

@injectable()
class UpdateQuantityItemUseCase {
    constructor(
        @inject('CartRepository')
        private cartRepository: ICartRepository,
        @inject('EcommerceRepository')
        private ecommerceRepository: IEcommerceRepository
    ) {}

    async execute({
        ecommerceId,
        clientId,
        planId,
        newQuantity
    }: IRequestUpdateQuantityItem) {
        const ecommerceExists =
            await this.ecommerceRepository.checkEcommerceExistence(ecommerceId);

        if (!ecommerceExists)
            throw new AppError(
                [
                    {
                        message: 'ecommerce not found',
                        code: 'addCartItemQuantity.ecommerceNotFound'
                    }
                ],
                '404 Not Found'
            );

        const ecommerceStatus =
            await this.ecommerceRepository.checkEcommerceStatus(ecommerceId);

        if (!ecommerceStatus)
            throw new AppError(
                [
                    {
                        message: 'ecommerce not found',
                        code: 'addCartItemQuantity.ecommerceNotFound'
                    }
                ],
                '404 Not Found'
            );

        const checkCartExistence = await this.cartRepository.checkCartExistence(
            ecommerceId,
            clientId
        );

        if (!checkCartExistence)
            throw new AppError(
                [
                    {
                        message: 'cart not found',
                        code: 'removeItem.cartNotFound'
                    }
                ],
                '404 Not Found'
            );

        const itemExistence = await this.cartRepository.checkCartItemExistance(
            ecommerceId,
            clientId,
            planId
        );

        if (!itemExistence)
            throw new AppError(
                [
                    {
                        message: 'item not found',
                        code: 'removeItem.itemNotFound'
                    }
                ],
                '404 Not Found'
            );
        const itemQuantityUpdated =
            await this.cartRepository.updateItemQuantity(
                ecommerceId,
                clientId,
                planId,
                newQuantity
            );

        return itemQuantityUpdated;
    }
}

export { UpdateQuantityItemUseCase };
