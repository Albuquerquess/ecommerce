import { Request } from 'express';
import { Response } from '@b4-org/middleware-express/ExpressTypes';
import { container } from 'tsyringe';
import { UpdateQuantityItemUseCase } from './updateQuantityUseCase';

class UpdateQuantityItemController {
    async handle(request: Request, response: Response) {
        const {
            plan_id: planId,
            client_id: clientId,
            new_quantity: newQuantity,
            ecommerce_id: ecommerceId
        } = request.body;

        const updateItemUseCase = container.resolve(UpdateQuantityItemUseCase);

        const updateItem = await updateItemUseCase.execute({
            planId,
            clientId,
            ecommerceId,
            newQuantity
        });

        return response.json({
            result: {
                updated: updateItem
            }
        });
    }
}

export { UpdateQuantityItemController };
