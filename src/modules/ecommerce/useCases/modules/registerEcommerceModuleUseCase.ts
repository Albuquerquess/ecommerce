import { IEcommerceRepository } from '@modules/ecommerce/repository/IEcommerceRepository';
import { inject, injectable } from 'tsyringe';

@injectable()
class RegisterEcommerceModuleUseCase {
    constructor(
        @inject('EcommerceRepository')
        private ecommerceRepository: IEcommerceRepository
    ) {}

    async execute(title, slug, ownerId, ecommerceId) {
        const register = await this.ecommerceRepository.registerEcommerceModule(
            title,
            slug,
            ownerId,
            ecommerceId
        );

        return register;
    }
}

export { RegisterEcommerceModuleUseCase };
