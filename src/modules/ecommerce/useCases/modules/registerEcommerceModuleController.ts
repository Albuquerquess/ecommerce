import { Request } from 'express';
import { Response } from '@b4-org/middleware-express/ExpressTypes';
import { container } from 'tsyringe';
import { RegisterEcommerceModuleUseCase } from './registerEcommerceModuleUseCase';

class RegisterEcommerceModuleController {
    async handle(request: Request, response: Response) {
        const {
            title,
            slug,
            owner_id: ownerId,
            ecommerce_id: ecommerceId
        } = request.body;

        const registerEcommerceModuleUseCase = container.resolve(
            RegisterEcommerceModuleUseCase
        );

        const register = await registerEcommerceModuleUseCase.execute(
            title,
            slug,
            ecommerceId,
            ownerId
        );

        return response.json({
            result: {
                register
            }
        });
    }
}

export { RegisterEcommerceModuleController };
