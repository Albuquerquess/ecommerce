import { IEcommerceProductsRepository } from '@modules/ecommerce/repository/IEcommerceProductsRepository';
import { inject, injectable } from 'tsyringe';

@injectable()
class IndexProductsByPageUseCase {
    constructor(
        @inject('EcommerceProductsRepository')
        private ecommerceProductsRepository: IEcommerceProductsRepository
    ) {}
    // TODO: implementar método que verifica se o ecommerce existe
    async execute(page, ecommerceId, perPage): Promise<any> {
        const products: object[] =
            await this.ecommerceProductsRepository.indexProductsByPage(
                page,
                ecommerceId,
                perPage
            );

        return products;
    }
}

export { IndexProductsByPageUseCase };
