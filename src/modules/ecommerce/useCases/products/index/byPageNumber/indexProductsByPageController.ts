import { container } from 'tsyringe';
import { Request } from 'express';
import { Response } from '@b4-org/middleware-express/ExpressTypes';
import { IRequestIndexProductsByPage } from '../../../../types/products.d';
import { IndexProductsByPageUseCase } from './indexProductsByPageUseCase';

class IndexProductsByPageController {
    async handle(request: Request, response: Response) {
        const {
            page,
            ecommerce_id: ecommerceId,
            per_page: perPage
        } = request.query as unknown as IRequestIndexProductsByPage;

        const indexProductsByPageUseCase = container.resolve(
            IndexProductsByPageUseCase
        );

        const products = await indexProductsByPageUseCase.execute(
            page,
            ecommerceId,
            perPage
        );

        return response.json({
            result: {
                products
            }
        });
    }
}

export { IndexProductsByPageController };
