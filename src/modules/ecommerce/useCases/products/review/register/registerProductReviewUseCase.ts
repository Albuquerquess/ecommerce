import { IEcommerceProductsRepository } from '@modules/ecommerce/repository/IEcommerceProductsRepository';
import { inject, injectable } from 'tsyringe';

@injectable()
class RegisterProductReviewUseCase {
    constructor(
        @inject('EcommerceProductsRepository')
        private ecommerceProductsRepository: IEcommerceProductsRepository
    ) { }

    async execute(
        productId: string,
        imageSrc: string,
        comment: string,
        ecommerceID: string
    ) {
        const registerProductReview =
            await this.ecommerceProductsRepository.registerProductReview(
                productId,
                imageSrc,
                ecommerceID,
                comment
            );
        return registerProductReview;
    }
}

export { RegisterProductReviewUseCase };
