import { Request } from 'express';
import { Response } from '@b4-org/middleware-express/ExpressTypes';
import { container, injectable } from 'tsyringe';
import { RegisterProductReviewUseCase } from './registerProductReviewUseCase';

@injectable()
class RegisterProductReviewController {
    async handle(request: Request, response: Response) {
        const {
            product_id: productId,
            image_src: imageSrc,
            ecommerce_id: ecommerceID,
            comment
        } = request.body;

        const registerProductReview = container.resolve(
            RegisterProductReviewUseCase
        );

        const register = await registerProductReview.execute(
            productId,
            imageSrc,
            ecommerceID,
            comment
        );

        return response.status(204).json({
            result: {
                register
            }
        });
    }
}

export { RegisterProductReviewController };
