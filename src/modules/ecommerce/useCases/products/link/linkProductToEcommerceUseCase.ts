import { AppError } from '@b4-org/middleware-express';
import { IEcommerceProductsRepository } from '@modules/ecommerce/repository/IEcommerceProductsRepository';
import { IEcommerceRepository } from '@modules/ecommerce/repository/IEcommerceRepository';
import { inject, injectable } from 'tsyringe';

@injectable()
class LinkProductToEcommerceUseCase {
    constructor(
        @inject('EcommerceProductsRepository')
        private ecommerceProductsRepository: IEcommerceProductsRepository,
        @inject('EcommerceRepository')
        private ecommerceRepository: IEcommerceRepository
    ) {}
    async execute(productId: number, ownerId: number, ecommerceId: number) {
        const productExists =
            await this.ecommerceProductsRepository.checkProductExistance(
                productId
            );

        if (!productExists)
            throw new AppError(
                [
                    {
                        message: 'product not found',
                        code: 'linkProductToEcommerce.productNotFound'
                    }
                ],
                '404 Not Found'
            );

        const checkUserOwnsTheProducts =
            await this.ecommerceProductsRepository.checkUserOwnsTheProducts(
                ownerId,
                [productId]
            );

        if (!checkUserOwnsTheProducts)
            throw new AppError(
                [
                    {
                        message:
                            'one or more products do not belong to the user.',
                        code: 'linkProductToEcommerce.productNotFound'
                    }
                ],
                '404 Not Found'
            );

        const ecommerceExists =
            await this.ecommerceRepository.checkEcommerceExistence(ecommerceId);

        if (!ecommerceExists)
            throw new AppError(
                [
                    {
                        message: 'ecommerce not found',
                        code: 'linkProductToEcommerce.ecommerceNotFound'
                    }
                ],
                '404 Not Found'
            );

        const ecommerceStatus =
            await this.ecommerceRepository.checkEcommerceStatus(ecommerceId);

        if (!ecommerceStatus)
            throw new AppError(
                [
                    {
                        message: 'ecommerce not found',
                        code: 'linkProductToEcommerce.ecommerceNotFound'
                    }
                ],
                '404 Not Found'
            );

        const userIsEcommerceOwner =
            await this.ecommerceRepository.checkEcommerceOwner(
                ecommerceId,
                ownerId
            );

        if (!userIsEcommerceOwner)
            throw new AppError(
                [
                    {
                        message:
                            'User does not have permission for this action',
                        code: 'linkProductToEcommerce.unauthorizedUser'
                    }
                ],
                '401 Unauthorized'
            );

        const linkExists =
            await this.ecommerceProductsRepository.checkProductLink(
                productId,
                ecommerceId
            );

        if (linkExists)
            throw new AppError(
                [
                    {
                        message: 'product link already exists',
                        code: 'linkProductToEcommerceUseCase.productAlreadyLinked'
                    }
                ],
                '409 Conflict'
            );

        const linked =
            await this.ecommerceProductsRepository.linkProductToEcommerce(
                ecommerceId,
                productId
            );

        if (!linked)
            throw new AppError(
                [
                    {
                        message:
                            'Cannot create link between product and ecommerce',
                        code: 'linkProductToEcommerceUseCase.cannotCreateLink'
                    }
                ],
                '500 Internal Server Error'
            );
        return linked;
    }
}

export { LinkProductToEcommerceUseCase };
