import { container } from 'tsyringe';
import { Response } from '@b4-org/middleware-express';
import { Request } from 'express';
import { LinkProductToEcommerceUseCase } from './linkProductToEcommerceUseCase';

class LinkProductToEcommerceController {
    async handle(request: Request, response: Response) {
        const {
            product_id: productId,
            owner_id: ownerId,
            ecommerce_id: ecommerceId
        } = request.body;

        const linkProductToEcommerceUseCase = container.resolve(
            LinkProductToEcommerceUseCase
        );

        const linked = await linkProductToEcommerceUseCase.execute(
            productId,
            ownerId,
            ecommerceId
        );

        return response.json({
            result: {
                linked
            }
        });
    }
}

export { LinkProductToEcommerceController };
