import { Response } from '@b4-org/middleware-express/ExpressTypes';
import { Request } from 'express';
import { container } from 'tsyringe';
import { IRequestRegisterNewProductOption } from '../../../types/products';
import { RegisterProductOptionUseCase } from './registerProductOptionUseCase';

class RegisterProductOptionController {
    async handle(request: Request, response: Response) {
        const {
            owner_id: ownerId,
            products
        }: IRequestRegisterNewProductOption = request.body;

        const registerProductOptionUseCase = container.resolve(
            RegisterProductOptionUseCase
        );

        const registered = await registerProductOptionUseCase.execute(
            ownerId,
            products
        );

        return response.json({
            result: {
                registered
            }
        });
    }
}

export { RegisterProductOptionController };
