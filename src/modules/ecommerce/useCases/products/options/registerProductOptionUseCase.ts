import { AppError } from '@b4-org/middleware-express';
import { IEcommerceProductsRepository } from '@modules/ecommerce/repository/IEcommerceProductsRepository';
import { INewProduct } from '@modules/ecommerce/types/products';
import { inject, injectable } from 'tsyringe';

@injectable()
class RegisterProductOptionUseCase {
    constructor(
        @inject('EcommerceProductsRepository')
        private ecommerceProductsRepository: IEcommerceProductsRepository
    ) {}
    async execute(ownerId: number, products: INewProduct[]): Promise<any> {
        const productsIds = products.map(product => product.product_id);

        const checkUserCanCreateProduct =
            await this.ecommerceProductsRepository.checkUserCanCreateProduct(
                ownerId
            );

        if (!checkUserCanCreateProduct)
            throw new AppError(
                [
                    {
                        message: 'user cannot create products',
                        code: 'registerProductOptionUseCase.userCannotCreateProducts'
                    }
                ],
                '401 Unauthorized'
            );

        const checkUserOwnsTheProducts =
            await this.ecommerceProductsRepository.checkUserOwnsTheProducts(
                ownerId,
                productsIds
            );

        if (!checkUserOwnsTheProducts)
            throw new AppError(
                [
                    {
                        message:
                            'one or more products do not belong to the user.',
                        code: 'registerProduct.userNotOwner'
                    }
                ],
                '401 Unauthorized'
            );

        await this.ecommerceProductsRepository.registerProduct(products);

        return true;
    }
}

export { RegisterProductOptionUseCase };
