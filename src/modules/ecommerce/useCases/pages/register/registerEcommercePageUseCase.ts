import { IEcommerceRepository } from '@modules/ecommerce/repository/IEcommerceRepository';
import { inject, injectable } from 'tsyringe';

@injectable()
class RegisterEcommercePageUseCase {
    constructor(
        @inject('EcommerceRepository')
        private ecommerceRepository: IEcommerceRepository
    ) {}

    async execute(ecommerceId, slug, title, body): Promise<any> {
        const register = await this.ecommerceRepository.registerEcommercePage(
            ecommerceId,
            slug,
            title,
            body
        );

        return register;
    }
}

export { RegisterEcommercePageUseCase };
