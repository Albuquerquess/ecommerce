import { Request } from 'express';
import { Response } from '@b4-org/middleware-express/ExpressTypes';
import { container } from 'tsyringe';
import { RegisterEcommercePageUseCase } from './registerEcommercePageUseCase';

class RegisterEcommercePageController {
    async handle(request: Request, response: Response): Promise<any> {
        const { ecommerce_id: ecommerceId, slug, title, body } = request.body;

        const registerEcommercePageUseCase = container.resolve(
            RegisterEcommercePageUseCase
        );

        const register = await registerEcommercePageUseCase.execute(
            ecommerceId,
            slug,
            title,
            body
        );
        return response.json({
            result: {
                registered: register
            }
        });
    }
}

export { RegisterEcommercePageController };
