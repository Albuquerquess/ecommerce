import { IEcommerceRepository } from '@modules/ecommerce/repository/IEcommerceRepository';
import { inject, injectable } from 'tsyringe';

@injectable()
class UpdateEcommerceTemplateUseCase {
    constructor(
        @inject('EcommerceRepository')
        private ecommerceRepository: IEcommerceRepository
    ) {}
    async execute(
        newEcommerceTemplate: string,
        ownerId: string,
        ecommerceId: string
    ) {
        const updateEcommerceTemplate =
            await this.ecommerceRepository.updateEcommerceTemplate(
                newEcommerceTemplate,
                ownerId,
                ecommerceId
            );

        return updateEcommerceTemplate;
    }
}

export { UpdateEcommerceTemplateUseCase };
