import { Request } from 'express';
import { Response } from '@b4-org/middleware-express/ExpressTypes';
import { container } from 'tsyringe';
import { UpdateEcommerceTemplateUseCase } from './updateEcommerceTemplateUseCase';

class UpdateEcommerceTemplateController {
    async handle(request: Request, response: Response): Promise<any> {
        const {
            new_ecommerce_template: newEcommerceTemplate,
            owner_id: ownerId,
            ecommerce_id: ecommerceId
        } = request.body;

        const updateEcommerceTemplate = container.resolve(
            UpdateEcommerceTemplateUseCase
        );

        const updateTemplate = await updateEcommerceTemplate.execute(
            newEcommerceTemplate,
            ownerId,
            ecommerceId
        );

        return response.json({
            result: {
                updated: updateTemplate
            }
        });
    }
}

export { UpdateEcommerceTemplateController };
