import { container } from 'tsyringe';
import { Request } from 'express';
import { Response } from '@b4-org/middleware-express/ExpressTypes';
import { DeleteEcommerceUseCase } from './deleteEcommerceUseCase';

interface IDeleteEcommerceRequest {
    ecommerce_id: number;
    owner_id: number;
}

class DeleteEcommerceController {
    async handle(request: Request, response: Response) {
        const { ecommerce_id: ecommerceId, owner_id: ownerId } =
            request.query as unknown as IDeleteEcommerceRequest;

        const deleteEcommerceUseCase = container.resolve(
            DeleteEcommerceUseCase
        );

        const deleted = await deleteEcommerceUseCase.execute(
            ecommerceId,
            ownerId
        );

        return response.json({
            result: {
                deleted
            }
        });
    }
}

export { DeleteEcommerceController };
