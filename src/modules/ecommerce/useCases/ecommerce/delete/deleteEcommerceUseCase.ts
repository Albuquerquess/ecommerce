import { AppError } from '@b4-org/middleware-express';
import { IEcommerceRepository } from '@modules/ecommerce/repository/IEcommerceRepository';
import { inject, injectable } from 'tsyringe';

@injectable()
class DeleteEcommerceUseCase {
    constructor(
        @inject('EcommerceRepository')
        private ecommerceRepository: IEcommerceRepository
    ) {}

    async execute(ecommerceId: number, ownerId: number) {
        const userIsOwner = await this.ecommerceRepository.checkEcommerceOwner(
            ecommerceId,
            ownerId
        );

        if (!userIsOwner) {
            throw new AppError(
                [
                    {
                        message:
                            'User does not have permission for this action',
                        code: 'deleteEcommerce.unauthorizedUser'
                    }
                ],
                '401 Unauthorized'
            );
        }

        const ecommerceStatus =
            await this.ecommerceRepository.checkEcommerceStatus(ecommerceId);

        if (!ecommerceStatus)
            throw new AppError(
                [
                    {
                        message: 'ecommerce not found',
                        code: 'deleteEcommerce.ecommerceNotFound'
                    }
                ],
                '404 Not Found'
            );

        const deleteEcommerce = await this.ecommerceRepository.deleteEcommerce(
            ecommerceId
        );
        if (!deleteEcommerce) {
            throw new AppError(
                [
                    {
                        message: 'cannot delete ecommerce, try again later.',
                        code: 'deleteEcommerce.cannotChangeStatus'
                    }
                ],
                '500 Internal Server Error'
            );
        }

        return deleteEcommerce;
    }
}

export { DeleteEcommerceUseCase };
