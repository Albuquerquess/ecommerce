import { Request } from 'express';
import { Response } from '@b4-org/middleware-express/ExpressTypes';
import { container } from 'tsyringe';
import { UpdateEcommerceTitleUseCase } from './updateEcommerceTitleUseCase';

class UpdateEcommerceTitleController {
    async handle(request: Request, response: Response) {
        const {
            new_ecommerce_title: newEcommerceTitle,
            owner_id: ownerId,
            ecommerce_id: ecommerceId
        } = request.body;

        const updateEcommerceTitle = container.resolve(
            UpdateEcommerceTitleUseCase
        );

        const update = await updateEcommerceTitle.execute(
            newEcommerceTitle,
            ownerId,
            ecommerceId
        );

        return response.json({
            result: {
                update
            }
        });
    }
}

export { UpdateEcommerceTitleController };
