import { IEcommerceRepository } from '@modules/ecommerce/repository/IEcommerceRepository';
import { inject, injectable } from 'tsyringe';

@injectable()
class UpdateEcommerceTitleUseCase {
    constructor(
        @inject('EcommerceRepository')
        private ecommerceRepository: IEcommerceRepository
    ) {}
    async execute(
        newEcommerceTitle: string,
        ownerId: string,
        ecommerceId: string
    ) {
        const updateEcommerceTItle =
            await this.ecommerceRepository.updateEcommerceTitle(
                newEcommerceTitle,
                ownerId,
                ecommerceId
            );

        return updateEcommerceTItle;
    }
}

export { UpdateEcommerceTitleUseCase };
