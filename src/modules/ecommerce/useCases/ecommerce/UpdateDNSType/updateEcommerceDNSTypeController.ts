import { Request } from 'express';
import { Response } from '@b4-org/middleware-express/ExpressTypes';
import { container } from 'tsyringe';
import { UpdateEcommerceDNSTypeUseCase } from './updateEcommerceDNSTypeUseCase';

class UpdateEcommerceDNSTypeController {
    async handle(request: Request, response: Response) {
        const {
            new_ecommerce_dns_type: newEcommerceDNSType,
            owner_id: ownerId,
            ecommerce_id: ecommerceId
        } = request.body;

        const updateEcommerceDNSType = container.resolve(
            UpdateEcommerceDNSTypeUseCase
        );

        const update = await updateEcommerceDNSType.execute(
            newEcommerceDNSType,
            ownerId,
            ecommerceId
        );

        return response.json({
            result: {
                update
            }
        });
    }
}

export { UpdateEcommerceDNSTypeController };
