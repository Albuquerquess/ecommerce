import { IEcommerceRepository } from '@modules/ecommerce/repository/IEcommerceRepository';
import { inject, injectable } from 'tsyringe';

@injectable()
class UpdateEcommerceDNSTypeUseCase {
    constructor(
        @inject('EcommerceRepository')
        private ecommerceRepository: IEcommerceRepository
    ) {}
    async execute(
        newEcommerceDNSType: string,
        ownerId: string,
        ecommerceId: string
    ) {
        const updateEcommerceDNSType =
            await this.ecommerceRepository.updateEcommerceDNSType(
                newEcommerceDNSType,
                ownerId,
                ecommerceId
            );

        return updateEcommerceDNSType;
    }
}

export { UpdateEcommerceDNSTypeUseCase };
