import { Request } from 'express';
import { Response } from '@b4-org/middleware-express/ExpressTypes';
import { container } from 'tsyringe';
import { UpdateEcommerceDomainUseCase } from './updateEcommerceDomainUseCase';

class UpdateEcommerceDomainController {
    async handle(request: Request, response: Response) {
        const {
            new_ecommerce_domain: newEcommerceDomain,
            owner_id: ownerId,
            ecommerce_id: ecommerceId
        } = request.body;

        const updateEcommerceDomain = container.resolve(
            UpdateEcommerceDomainUseCase
        );

        const updateDomain = await updateEcommerceDomain.execute(
            newEcommerceDomain,
            ownerId,
            ecommerceId
        );

        return response.json({
            result: {
                updateDomain
            }
        });
    }
}

export { UpdateEcommerceDomainController };
