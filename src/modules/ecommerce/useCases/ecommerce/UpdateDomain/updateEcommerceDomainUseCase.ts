import { IEcommerceRepository } from '@modules/ecommerce/repository/IEcommerceRepository';
import { inject, injectable } from 'tsyringe';

@injectable()
class UpdateEcommerceDomainUseCase {
    constructor(
        @inject('EcommerceRepository')
        private ecommerceRepository: IEcommerceRepository
    ) {}
    async execute(
        newEcommerceDomain: string,
        ownerId: string,
        ecommerceId: string
    ) {
        const updateEcommerceDomain =
            await this.ecommerceRepository.updateEcommerceDomain(
                newEcommerceDomain,
                ownerId,
                ecommerceId
            );

        return updateEcommerceDomain;
    }
}

export { UpdateEcommerceDomainUseCase };
