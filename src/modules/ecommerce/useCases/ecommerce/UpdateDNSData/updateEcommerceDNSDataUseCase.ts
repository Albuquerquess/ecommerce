import { IEcommerceRepository } from '@modules/ecommerce/repository/IEcommerceRepository';
import { inject, injectable } from 'tsyringe';

@injectable()
class UpdateEcommerceDNSDataUseCase {
    constructor(
        @inject('EcommerceRepository')
        private ecommerceRepository: IEcommerceRepository
    ) {}
    async execute(
        newEcommerceDNSData: string,
        ownerId: string,
        ecommerceId: string
    ) {
        const updateEcommerceDNSData =
            await this.ecommerceRepository.updateEcommerceDNSData(
                newEcommerceDNSData,
                ownerId,
                ecommerceId
            );

        return updateEcommerceDNSData;
    }
}

export { UpdateEcommerceDNSDataUseCase };
