import { Request } from 'express';
import { Response } from '@b4-org/middleware-express/ExpressTypes';
import { container } from 'tsyringe';
import { UpdateEcommerceDNSDataUseCase } from './updateEcommerceDNSDataUseCase';

class UpdateEcommerceDNSDataController {
    async handle(request: Request, response: Response) {
        const {
            new_ecommerce_dns_type: newEcommerceDNSData,
            owner_id: ownerId,
            ecommerce_id: ecommerceId
        } = request.body;

        const updateEcommerceDNSData = container.resolve(
            UpdateEcommerceDNSDataUseCase
        );

        const update = await updateEcommerceDNSData.execute(
            newEcommerceDNSData,
            ownerId,
            ecommerceId
        );

        return response.json({
            result: {
                update
            }
        });
    }
}

export { UpdateEcommerceDNSDataController };
