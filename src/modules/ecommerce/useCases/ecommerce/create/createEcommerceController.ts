import { Response } from '@b4-org/middleware-express/ExpressTypes';
import { Request } from 'express';
import { container } from 'tsyringe';
import { CreateEcommerceUseCase } from './createEcommerceUseCase';

class CreateEcommerceController {
    async handle(request: Request, response: Response) {
        const {
            title,
            domain,
            owner_id: ownerId,
            DNS,
            products,
            modules,
            pages,
            template,
            menus
        } = request.body;

        const createEcommerceUseCase = container.resolve(
            CreateEcommerceUseCase
        );
        const created = await createEcommerceUseCase.execute(
            title,
            domain,
            ownerId,
            DNS,
            template,
            menus
        );

        return response.json({
            result: {
                created
            }
        });
    }
}

export { CreateEcommerceController };
