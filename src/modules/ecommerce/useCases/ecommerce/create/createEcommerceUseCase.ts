import { IEcommerceRepository } from '@modules/ecommerce/repository/IEcommerceRepository';
import { IDns, IMenu, ITemplate } from '@modules/ecommerce/types/ecommerce';
import { inject, injectable } from 'tsyringe';

@injectable()
class CreateEcommerceUseCase {
    constructor(
        @inject('EcommerceRepository')
        private ecommerceRepository: IEcommerceRepository
    ) {}

    async execute(
        title: string,
        domain: string,
        ownerId: number,
        DNS: IDns,
        template: ITemplate,
        menus: IMenu[]
    ) {
        const ecommerceId = await this.ecommerceRepository.createEcommerce(
            title,
            domain,
            ownerId
        );
        await this.ecommerceRepository.createDNS(ecommerceId, DNS);
        // await this.ecommerceRepository.createTemplate(ecommerceId, template); // not implement
        await this.ecommerceRepository.createMenu(ecommerceId, menus);
        return true;
    }
}

export { CreateEcommerceUseCase };
