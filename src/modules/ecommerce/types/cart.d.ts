export interface IIndexItemsRequest {
    ecommerce_id: number;
    client_id: number;
}

export interface IRequestUpdateQuantityItem {
    ecommerceId: number;
    clientId: number;
    planId: number;
    newQuantity: number;
}
