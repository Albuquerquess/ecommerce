export interface IOptionsValues {
    value: string;
    plan_id: number;
}

export interface IProductoptions {
    title: string;
    display_order: number;
    values: IOptionsValues[];
}

export interface INewProduct {
    product_id: number;
    options: IProductoptions[];
}

export interface IRequestRegisterNewProductOption {
    owner_id: number;
    products: INewProduct[];
}
export interface IProductInfo {
    productId: number;
}

export interface IRequestIndexProductsByPage {
    page: number;
    ecommerce_id: number;
    per_page: number;
}
