interface IDns {
    type: 'A' | 'AAAA' | 'CNAME';
    data: string;
}

interface IProduct {
    title: string;
    productId: string;
    categoryId: string;
}

interface IModule {
    title: string;
    slug: string;
}

interface IPage {
    title: string;
    slug: string;
    body: string;
}

interface ITemplate {
    path: string;
}

interface ISubmenu {
    label: string;
    url: string;
}

interface IMenu {
    label: string;
    submenu: ISubmenu[];
}

export interface IRequestCreateEcommerce {
    title: string;
    domain: string;
    ownerId: number;
    dns: IDns;
    products: IProduct[];
    modules: IModule[];
    pages: IPage[];
    template: ITemplate;
    menus: IMenu[];
}

export interface ISelectEcommerceStatus {
    status: 1 | 0;
}
