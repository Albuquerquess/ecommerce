import { decodeID, encodeID } from '@b4-org/hash-id';
import { AppError } from '@b4-org/middleware-express';
import { IEcommerceProductsRepository } from '@modules/ecommerce/repository/IEcommerceProductsRepository';
import { INewProduct } from '@modules/ecommerce/types/products';
import { connection } from '@shared/infra/knex';

class EcommerceProductsRepository implements IEcommerceProductsRepository {
    async checkProductExistance(productId: number): Promise<boolean> {
        const [productExists] = await connection('products')
            .where('products.id', productId)
            .count();
        return productExists['count(*)'] > 0;
    }
    async checkUserOwnsTheProducts(
        ownerId: number,
        productsIds: number[]
    ): Promise<boolean> {
        const [checkProducerProducts] = await connection('products')
            .whereIn('products.id', productsIds)
            .where('uid', ownerId)
            .count();

        return checkProducerProducts['count(*)'] === String(productsIds.length);
    }
    async checkUserCanCreateProduct(ownerId: number): Promise<boolean> {
        const [userCanCreateProduct] = await connection('users')
            .where({
                id: ownerId,
                can_create_product: '1' // true
            })
            .count();
        return userCanCreateProduct['count(*)'] === '1';
    }
    async checkProductLink(
        productId: number,
        ecommerceId: number
    ): Promise<boolean> {
        const [productLinkExists] = await connection(
            'ecommerce_products_references'
        )
            .where({
                product_id: productId,
                ecommerce_id: ecommerceId
            })
            .count();
        return productLinkExists['count(*)'] > 0;
    }
    async registerProductReview(
        productId: string,
        imageSrc: string,
        comment: string,
        ecommerceId: string
    ): Promise<any> {
        const productIdDecoded = decodeID(productId);
        const ecommerceIdDecoded = decodeID(ecommerceId);
        if (!productIdDecoded || !ecommerceId)
            throw new AppError(
                [
                    {
                        message: 'Invalid product_id or ecommerce_id params',
                        code: 'registerProductReview.invalidParams'
                    }
                ],
                '400 Bad Request'
            );

        const trx = await connection.transaction();
        const [registerProductReviewId] = await trx(
            'ecommerce_products_review'
        ).insert({
            product_id: Number(productIdDecoded),
            image_src: imageSrc,
            ecommerce_id: Number(ecommerceIdDecoded),
            comment
        });

        if (!registerProductReviewId) {
            trx.rollback();
            throw new AppError(
                [
                    {
                        message: 'cannot register product review',
                        code: 'registerProductReview.RegisterError'
                    }
                ],
                '500 Internal Server Error'
            );
        }

        trx.commit();
        return Boolean(registerProductReviewId);
    }
    async registerProduct(products: INewProduct[]) {
        const trx = await connection.transaction();
        try {
            // eslint-disable-next-line no-restricted-syntax
            for await (const product of products) {
                // eslint-disable-next-line no-restricted-syntax
                for await (const option of product.options) {
                    const [optionId] = await trx('products_options').insert({
                        product_id: product.product_id,
                        title: option.title,
                        display_order: option.display_order
                    });

                    // eslint-disable-next-line no-restricted-syntax
                    for await (const value of option.values) {
                        const [optionValueId] = await trx(
                            'products_options_values'
                        ).insert({
                            product_id: product.product_id,
                            products_options_id: optionId,
                            value: value.value
                        });

                        await trx('products_plans_options_values').insert({
                            plan_id: value.plan_id,
                            products_options_id: optionId,
                            products_options_values_id: optionValueId
                        });
                    }
                }
            }

            trx.commit();
            return true;
        } catch (error) {
            trx.rollback();
            throw error;
        }
    }
    async indexProductsByPage(
        page: number,
        ecommerceId: number,
        limit: number
    ): Promise<object[]> {
        const products = await connection('ecommerce_products_references')
            .join(
                'products',
                'ecommerce_products_references.product_id',
                '=',
                'products.id'
            )
            .where('ecommerce_products_references.ecommerce_id', ecommerceId)
            .limit(limit)
            .select({
                productId: 'products.id',
                title: 'products.title',
                image: 'products.image',
                type: 'products.type',
                typePayment: 'products.type_payment',
                description: 'products.description',
                shippingType: 'products.shipping_type'
            });

        const productIds = products.map(product => product.productId);
        const options = await connection('products_options')
            .whereIn('products_options.product_id', productIds)
            .select({
                optionId: 'products_options.id',
                title: 'products_options.title',
                productId: 'products_options.product_id',
                displayOrder: 'products_options.display_order'
            });
        const optionsIds = options.map(option => option.optionId);

        const optionsValues = await connection('products_options_values')
            .whereIn('products_options_values.products_options_id', optionsIds)
            .select({
                optionValueId: 'products_options_values.id',
                productId: 'products_options_values.product_id',
                productsOptionsId:
                    'products_options_values.products_options_id',
                value: 'products_options_values.value',
                status: 'products_options_values.status'
            });

        const productsFormated = products.map(product => {
            return {
                ...product,
                productId: encodeID(product.productId),
                options: options
                    .map(option => {
                        return (
                            option.productId === product.productId && {
                                title: option.title,
                                displayOrder: option.displayOrder,
                                optionId: encodeID(option.optionId),
                                values: optionsValues.map(value => {
                                    if (value.productsOptionsId === option.optionId) {
                                        return {
                                            optionValueId: encodeID(value.optionValueId),
                                            value: value.value,
                                            status: value.status
                                        }
                                    }

                                }

                                ).filter(Boolean)
                            }
                        );
                    })
                    .filter(Boolean)
            };
        });

        return productsFormated;
    }
    async linkProductToEcommerce(
        ecommerceId: number,
        productId: number
    ): Promise<boolean> {
        const [linkId] = await connection(
            'ecommerce_products_references'
        ).insert({
            ecommerce_id: ecommerceId,
            product_id: productId
        });
        return linkId > 0;
    }
}

export { EcommerceProductsRepository };
