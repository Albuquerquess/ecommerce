import { decodeID } from '@shared/utils/HashIds';
import { AppError } from '@b4-org/middleware-express';

import { IEcommerceRepository } from '@modules/ecommerce/repository/IEcommerceRepository';
import {
    IMenu,
    ISelectEcommerceStatus,
    ITemplate
} from '@modules/ecommerce/types/ecommerce';
import { connection } from '@shared/infra/knex';

class EcommerceRepository implements IEcommerceRepository {
    async createEcommerce(
        title: string,
        domain: string,
        ownerId: number
    ): Promise<number> {
        const trx = await connection.transaction();

        const [ecommerceId] = await trx('ecommerce').insert({
            title,
            domain,
            owner_id: ownerId,
            status: true
        });

        trx.commit();
        return ecommerceId;
    }
    async createDNS(ecommerceId: number, Dns): Promise<boolean> {
        const [DNSId] = await connection('ecommerce_dns').insert({
            ecommerce_id: ecommerceId,
            type: Dns.type,
            data: Dns.data
        });

        return DNSId > 0;
    }
    async createTemplate(
        ecommerceId: number,
        template: ITemplate
    ): Promise<boolean> {
        const [templateId] = await connection('ecommerce_template').insert({
            ecommerce_id: ecommerceId,
            path: template.path
        });
        return templateId > 1;
    }
    async createMenu(ecommerceId: number, menus: IMenu[]): Promise<boolean> {
        const trx = await connection.transaction();
        try {
            // eslint-disable-next-line no-restricted-syntax
            for await (const menu of menus) {
                const [menuId] = await trx('ecommerce_menu').insert({
                    label: menu.label,
                    ecommerce_id: ecommerceId,
                    status: true
                });
                await trx('ecommerce_menu_submenu').insert(
                    menu.submenu.map(submenu => {
                        return {
                            ...submenu,
                            ecommerce_menu_id: menuId,
                            status: true
                        };
                    })
                );
            }
            trx.commit();
            return true;
        } catch (error) {
            trx.rollback();
            throw error;
        }
    }
    async checkEcommerceOwner(ecommerceId, ownerId) {
        const [checkEcommerceOwner] = await connection('ecommerce')
            .where({
                id: ecommerceId,
                owner_id: ownerId
            })
            .count();

        return checkEcommerceOwner['count(*)'] === '1';
    }

    async checkEcommerceExistence(ecommerceId: number): Promise<boolean> {
        const [ecommerceExists] = await connection('ecommerce')
            .where({
                id: ecommerceId
            })
            .count();
        return ecommerceExists['count(*)'] === '1';
    }
    async checkEcommerceStatus(ecommerceId) {
        const [ecommerceStatus]: ISelectEcommerceStatus[] = await connection(
            'ecommerce'
        )
            .where({
                id: ecommerceId
            })
            .select('status');
        return ecommerceStatus.status === 1;
    }
    async deleteEcommerce(ecommerceId: number): Promise<boolean> {
        const changeStatus = await connection('ecommerce')
            .where('ecommerce.id', ecommerceId)
            .limit(1)
            .update({ 'ecommerce.status': false });

        return changeStatus === 1;
    }
    async updateEcommerceTitle(
        newEcommerceTitle: string,
        ownerId: string,
        ecommerceId: string
    ) {
        const trx = await connection.transaction();

        const ownerIdDecoded = decodeID(ownerId);
        const ecommerceIdDecoded = decodeID(ecommerceId);

        if (!ownerIdDecoded || !ecommerceIdDecoded)
            throw new AppError(
                [
                    {
                        message: 'invalid ecommerce_id or owner_id params',
                        code: 'changesEcommerceTitle.invalidParams'
                    }
                ],
                '400 Bad Request'
            );

        if (typeof newEcommerceTitle !== 'string')
            throw new AppError(
                [
                    {
                        message: 'newEcommerceTitle must be a string!',
                        code: 'changesEcommerceTitle.invalidNewTitle'
                    }
                ],
                '400 Bad Request'
            );

        const newTitle = await trx('ecommerce')
            .where('commerce_id', Number(ecommerceIdDecoded))
            .andWhere('ecommerce.owner_id', Number(ownerId))
            .update({ 'ecommerce.title': newEcommerceTitle });

        if (newTitle !== 1) {
            trx.rollback();
            throw new AppError(
                [
                    {
                        message: 'cannot update ecommerce title!',
                        code: 'changesEcommerceTitle.updateEcommerceTitleError'
                    }
                ],
                '500 Internal Server Error'
            );
        }

        trx.commit();
        return newTitle;
    }
    async updateEcommerceDomain(
        newEcommerceDomain: string,
        ownerId: string,
        ecommerceId: string
    ) {
        const trx = await connection.transaction();
        const ownerIdDecoded = decodeID(ownerId);
        const ecommerceIdDecoded = decodeID(ecommerceId);

        if (!ownerIdDecoded || !ecommerceIdDecoded)
            throw new AppError(
                [
                    {
                        message: 'invalid ecommerce_id or owner_id params',
                        code: 'changesEcommerceDomain.invalidParams'
                    }
                ],
                '400 Bad Request'
            );

        if (typeof newEcommerceDomain !== 'string')
            throw new AppError(
                [
                    {
                        message: 'new_ecommerce_domain must be a string!',
                        code: 'changesEcommerceDomain.invalidNewDomain'
                    }
                ],
                '400 Bad Request'
            );

        const newDomain = await trx('ecommerce')
            .where('commerce.id', Number(ecommerceIdDecoded))
            .andWhere('ecommerce.owner_id', Number(ownerId))
            .update({ 'ecommerce.domain': newEcommerceDomain });

        if (newDomain !== 1) {
            trx.rollback();
            throw new AppError(
                [
                    {
                        message: 'cannot update ecommerce Domain!',
                        code: 'changesEcommerceDomain.updateEcommerceDomainError'
                    }
                ],
                '500 Internal Server Error'
            );
        }

        trx.commit();
        return newDomain === 1;
    }
    async updateEcommerceDNSType(
        newEcommerceDNSType: string,
        ownerId: string,
        ecommerceId: string
    ) {
        const trx = await connection.transaction();

        const ownerIdDecoded = decodeID(ownerId);
        const ecommerceIdDecoded = decodeID(ecommerceId);

        if (!ownerIdDecoded || !ecommerceIdDecoded)
            throw new AppError(
                [
                    {
                        message: 'invalid ecommerce_id or owner_id params',
                        code: 'changesEcommerceDNSType.invalidParams'
                    }
                ],
                '400 Bad Request'
            );

        if (typeof newEcommerceDNSType !== 'string')
            throw new AppError(
                [
                    {
                        message:
                            'new_ecommerce_dns_type param must be a string!',
                        code: 'updateEcommerceDNSType.invalidNewDomain'
                    }
                ],
                '400 Bad Request'
            );

        const newDNSType = await trx('ecommerce')
            .join(
                'ecommerce_dns',
                'ecommerce.id',
                '=',
                'ecommerce_dns.ecommerce_id'
            )
            .where('commerce_id', Number(ecommerceIdDecoded))
            .andWhere('ecommerce.owner_id', Number(ownerId))
            .update({ 'ecommerce_dns.type': newEcommerceDNSType });

        if (newDNSType !== 1) {
            trx.rollback();
            throw new AppError(
                [
                    {
                        message: 'cannot update ecommerce Domain!',
                        code: 'updateEcommerceDNSType.updateEcommerceDNSTypeError'
                    }
                ],
                '500 Internal Server Error'
            );
        }

        return newDNSType === 1;
    }
    async updateEcommerceDNSData(
        newEcommerceDNSData: string,
        ownerId: string,
        ecommerceId: string
    ) {
        const trx = await connection.transaction();

        const ownerIdDecoded = decodeID(ownerId);
        const ecommerceIdDecoded = decodeID(ecommerceId);

        if (!ownerIdDecoded || !ecommerceIdDecoded)
            throw new AppError(
                [
                    {
                        message: 'invalid ecommerce_id or owner_id params',
                        code: 'updateEcommerceDNSData.invalidParams'
                    }
                ],
                '400 Bad Request'
            );

        if (typeof newEcommerceDNSData !== 'string')
            throw new AppError(
                [
                    {
                        message:
                            'new_ecommerce_dns_data param must be a string!',
                        code: 'changesEcommerceDNSData.invalidNewDNSData'
                    }
                ],
                '400 Bad Request'
            );

        const newDNSData = await trx('ecommerce')
            .join(
                'ecommerce_dns',
                'ecommerce.id',
                '=',
                'ecommerce_dns.ecommerce_id'
            )
            .where('ecommerce_id', Number(ecommerceIdDecoded))
            .andWhere('ecommerce.owner_id', Number(ownerId))
            .update({ 'ecommerce_dns.data': newEcommerceDNSData });

        if (newDNSData !== 1) {
            trx.rollback();
            throw new AppError(
                [
                    {
                        message: 'cannot update ecommerce Domain!',
                        code: 'changesEcommerceDomain.updateEcommerceDomainError'
                    }
                ],
                '500 Internal Server Error'
            );
        }

        return newDNSData === 1;
    }
    async updateEcommerceTemplate(
        newEcommerceTemplate: string,
        ownerId: string,
        ecommerceId: string
    ) {
        const trx = await connection.transaction();

        const ownerIdDecoded = decodeID(ownerId);
        const ecommerceIdDecoded = decodeID(ecommerceId);

        if (!ownerIdDecoded || !ecommerceIdDecoded)
            throw new AppError(
                [
                    {
                        message: 'invalid ecommerce_id or owner_id params',
                        code: 'updateEcommerceTemplate.invalidParams'
                    }
                ],
                '400 Bad Request'
            );

        if (typeof newEcommerceTemplate !== 'string')
            throw new AppError(
                [
                    {
                        message:
                            'new_ecommerce_template param must be a string!',
                        code: 'UpdateEcommerceTemplate.invalidTemplate'
                    }
                ],
                '400 Bad Request'
            );

        const newTemplate = await trx('ecommerce')
            .join(
                'ecommerce_template',
                'ecommerce.id',
                '=',
                'ecommerce_template.ecommerce_id'
            )
            .where('commerce_id', Number(ecommerceIdDecoded))
            .andWhere('ecommerce.owner_id', Number(ownerId))
            .update({ 'ecommerce_template.path': newEcommerceTemplate });

        if (newTemplate !== 1)
            throw new AppError(
                [
                    {
                        message: 'cannot update ecommerce template!',
                        code: 'updateEcommerceTemplate.updateEcommerceTemplateError'
                    }
                ],
                '500 Internal Server Error'
            );

        return newTemplate === 1;
    }
    async registerEcommerceModule(title, slug, ecommerceId, ownerId) {
        const ecommerceIdDecoded = decodeID(ecommerceId);
        const ownerIdDecoded = decodeID(ownerId);

        if (!ecommerceIdDecoded || !ownerId)
            throw new AppError(
                [
                    {
                        message: 'invalid ecommerce_id ow owner_id params',
                        code: 'registerEcommerceModule.invalidParams'
                    }
                ],
                '400 Bad Request'
            );
        const trx = await connection.transaction();

        const [verifyUserExistance] = await trx('ecommerce')
            .where('ecommerce.id', Number(ecommerceIdDecoded))
            .andWhere('ecommerce.owner_id', Number(ownerIdDecoded))
            .count();

        if (verifyUserExistance['count(*)'] === 0)
            throw new AppError(
                [
                    {
                        message:
                            'user does not exist or does not have permission to register an module!!',
                        code: 'registerEcommerceModule.UserDoesNotExistOrNotAccredited'
                    }
                ],
                '409 Conflict'
            );
        const [newModule] = await trx('ecommerce_modules')
            .join(
                'ecommerce',
                'ecommerce_modules.ecommerce_id',
                '=',
                'ecommerce.id'
            )
            .where('ecommerce_modules.ecommerce_id', Number(ecommerceIdDecoded))
            .andWhere('ecommerce.owner_id', Number(ownerIdDecoded));
    }
    async registerEcommercePage(ecommerceId, slug, title, body) {
        const ecommerceIdDecoded = decodeID(ecommerceId);

        if (!ecommerceIdDecoded)
            throw new AppError(
                [
                    {
                        message: 'invalid ecommerce_id param!',
                        code: 'registerEcommercePage.invalidParams'
                    }
                ],
                '400 Bad Request'
            );

        const trx = await connection.transaction();

        const [verifyEcommerceExistance] = await trx('ecommerce')
            .where('ecommerce.id', Number(ecommerceIdDecoded))
            .count();

        if (verifyEcommerceExistance['count(*)'] === 0)
            throw new AppError(
                [
                    {
                        message: 'ecommerce not found',
                        code: 'registerEcommercePage.ecommerceNotFound'
                    }
                ],
                '404 Not Found'
            );
        const [newPage] = await trx('ecommerce_modules').insert({
            ecommerce_id: ecommerceIdDecoded,
            slug,
            body,
            title
        });

        if (!newPage)
            throw new AppError(
                [
                    {
                        message: 'cannot create page',
                        code: 'registerEcommercePage.insertError'
                    }
                ],
                '500 Internal Server Error'
            );
        return newPage > 0;
    }
}

export { EcommerceRepository };
