import { encodeID, decodeID } from '@b4-org/hash-id';
import { AppError } from '@b4-org/middleware-express';
import { ICartRepository } from '@modules/ecommerce/repository/ICartRepository';
import { connection } from '@shared/infra/knex';

class CartRepository implements ICartRepository {
    async checkCartExistence(
        ecommerceId: number,
        clientId: number
    ): Promise<boolean> {
        const [cart] = await connection('ecommerce_cart')
            .where('ecommerce_cart.client_id', clientId)
            .andWhere('ecommerce_cart.ecommerce_id', ecommerceId)
            .count();
        return cart['count(*)'] === '1';
    }
    async checkCartItemExistance(
        ecommerceId: number,
        clientId: number,
        planId: number
    ): Promise<boolean> {
        const [cart] = await connection('ecommerce_cart')
            .where({
                client_id: clientId,
                ecommerce_id: ecommerceId
            })
            .select('id');
        const [itemExistence] = await connection('ecommerce_cart_products')
            .join(
                'ecommerce_cart',
                'ecommerce_cart.id',
                '=',
                'ecommerce_cart_products.cart_id'
            )
            .where('ecommerce_cart.id', cart.id)
            .andWhere('ecommerce_cart.client_id', clientId)
            .andWhere('ecommerce_cart.ecommerce_id', ecommerceId)
            .andWhere('ecommerce_cart_products.plan_id', planId)
            .count();
        return itemExistence['count(*)'] === '1';
    }
    async createCart(ecommerceId: number, clientId: number): Promise<number> {
        const [cartId] = await connection('ecommerce_cart').insert({
            ecommerce_id: ecommerceId,
            client_id: clientId
        });
        return cartId;
    }
    async addItemToCart(
        planId: number,
        quantity: number,
        ecommerceId: number,
        clientId: number
    ): Promise<any> {
        const [cart] = await connection('ecommerce_cart')
            .where({
                ecommerce_id: ecommerceId,
                client_id: clientId
            })
            .select('id');

        const [cartProducts] = await connection(
            'ecommerce_cart_products'
        ).insert({
            quantity,
            cart_id: cart.id,
            plan_id: planId
        });

        return cartProducts > 0;
    }
    async removeItemFromCart(
        ecommerceId: number,
        clientId: number,
        planId: number
    ): Promise<boolean> {
        const deleted = await connection('ecommerce_cart_products')
            .join(
                'ecommerce_cart',
                'ecommerce_cart_products.cart_id',
                '=',
                'ecommerce_cart.id'
            )
            .where('ecommerce_cart.ecommerce_id', ecommerceId)
            .andWhere('ecommerce_cart.client_id', clientId)
            .andWhere('ecommerce_cart_products.plan_id', planId)
            .del()
            .limit(1);

        return deleted === 1;
    }
    async indexCartItems(ecommerceId: string, clientId: string) {
        const cart = await connection('ecommerce_cart')
            .join(
                'ecommerce_cart_products',
                'ecommerce_cart.id',
                '=',
                'ecommerce_cart_products.cart_id'
            )
            .join(
                'products_plans',
                'ecommerce_cart_products.plan_id',
                '=',
                'products_plans.id'
            )
            .where('ecommerce_cart.ecommerce_id', ecommerceId)
            .andWhere('ecommerce_cart.client_id', clientId)
            .select({
                planId: 'ecommerce_cart_products.plan_id',
                quantity: 'ecommerce_cart_products.quantity',
                title: 'products_plans.title',
                dimensions: 'products_plans.dimensions',
                image: 'products_plans.image',
                amount: 'products_plans.amount',
                amountFirstInstallment:
                    'products_plans.amount_first_installment'
            });

        const cartFormated = cart.map(cartItem => {
            return {
                ...cartItem,
                planId: encodeID(cartItem.planId)
            };
        });

        return {
            ecommerce: {
                cliente_id: clientId,
                ecommerce_id: ecommerceId
            },
            cart: cartFormated
        };
    }
    async updateItemQuantity(
        ecommerceId: number,
        clientId: number,
        planId: number,
        newQuantity: number
    ) {
        const quantityItemUpdate = await connection('ecommerce_cart_products')
            .join(
                'ecommerce_cart',
                'ecommerce_cart_products.cart_id',
                '=',
                'ecommerce_cart.id'
            )
            .andWhere('ecommerce_cart.client_id', clientId)
            .andWhere('ecommerce_cart.ecommerce_id', ecommerceId)
            .andWhere('ecommerce_cart_products.plan_id', planId)
            .update('ecommerce_cart_products.quantity', newQuantity)
            .limit(1);

        return quantityItemUpdate === 1;
    }
}

export { CartRepository };
