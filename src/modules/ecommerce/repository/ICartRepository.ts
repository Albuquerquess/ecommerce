interface ICartRepository {
    checkCartExistence(ecommerceId, clientId): Promise<boolean>;
    checkCartItemExistance(ecommerceId, clientId, planId): Promise<boolean>;
    createCart(ecommerceId, clientId): Promise<number>;
    addItemToCart(ecommerceId, clientId, planId, quantity): Promise<boolean>;
    removeItemFromCart(ecommerceId, clientId, planId): Promise<boolean>;
    indexCartItems(ecommerceId, cartId): Promise<any>;
    updateItemQuantity(
        ecommerceId,
        clientId,
        planId,
        newQuantity
    ): Promise<boolean>;
}

export { ICartRepository };
