import { INewProduct } from '../types/products';

interface IEcommerceProductsRepository {
    checkUserOwnsTheProducts(
        ownerId: number,
        productsIds: number[]
    ): Promise<boolean>;
    checkUserCanCreateProduct(ownerId: number): Promise<boolean>;
    checkProductExistance(productId: number): Promise<boolean>;
    checkProductLink(productId: number, ecommerceId: number): Promise<boolean>;
    registerProductReview(
        productId: string,
        imageSrc: string,
        ecommerceId: string,
        comment: string
    ): Promise<any>;
    registerProduct(products: INewProduct[]): Promise<boolean>;
    linkProductToEcommerce(
        ecommerceId: number,
        productId: number
    ): Promise<boolean>;
    indexProductsByPage(
        page: number,
        ecommerceId: number,
        limit: number
    ): Promise<object[]>;
}

export { IEcommerceProductsRepository };
