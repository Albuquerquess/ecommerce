import { IDns, IMenu, ITemplate } from '../types/ecommerce';

interface IEcommerceRepository {
    checkEcommerceOwner(ecommerceId: number, ownerId: number): Promise<boolean>;
    checkEcommerceStatus(ecommerceId: number): Promise<boolean>;
    checkEcommerceExistence(ecommerceId: number): Promise<boolean>;
    createEcommerce(
        title: string,
        domain: string,
        ownerId: number
    ): Promise<number>;
    createDNS(ecommerceId: number, DNS: IDns): Promise<boolean>;
    createTemplate(ecommerceId: number, template: ITemplate): Promise<boolean>;
    createMenu(ecommerceId: number, menus: IMenu[]): Promise<boolean>;
    deleteEcommerce(ecommerceId: number): Promise<boolean>;
    updateEcommerceTitle(
        newEcommerceTitle: string,
        ownerId: string,
        ecommerceId: string
    ): Promise<any>;
    updateEcommerceDomain(
        newEcommerceDomain: string,
        ownerId: string,
        ecommerceId: string
    ): Promise<any>;
    updateEcommerceDNSType(
        newEcommerceDNSType: string,
        ownerId: string,
        ecommerceId: string
    ): Promise<any>;
    updateEcommerceDNSData(
        newEcommerceDNSData: string,
        ownerId: string,
        ecommerceId: string
    ): Promise<any>;
    updateEcommerceTemplate(
        newEcommerceTemplate: string,
        ownerId: string,
        ecommerceId: string
    ): Promise<any>;
    registerEcommerceModule(
        title: string,
        slug: string,
        ownerId: string,
        ecommerceId: string
    ): Promise<any>;
    registerEcommercePage(
        ecommerceId: string,
        slug: string,
        title: string,
        body: string
    ): Promise<any>;
}

export { IEcommerceRepository };
