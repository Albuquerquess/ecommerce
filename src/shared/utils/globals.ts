/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable no-extend-native */
declare global {
    interface Number {
        toDecimal(precision?: number): number;
    }
    interface String {
        toDecimal(precision?: number): number;
    }
}

String.prototype.toDecimal = function toDecimal(precision = 2) {
    return Number(Number(this).toFixed(precision));
};
Number.prototype.toDecimal = function toDecimal(precision = 2) {
    return Number(Number(this).toFixed(precision));
};

export { };
