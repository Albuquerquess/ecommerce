import { container } from 'tsyringe';

import { ICartRepository } from '@modules/ecommerce/repository/ICartRepository';
import { CartRepository } from '@modules/ecommerce/infra/knex/repositories/CartRepository';
import { IEcommerceRepository } from '@modules/ecommerce/repository/IEcommerceRepository';
import { EcommerceRepository } from '@modules/ecommerce/infra/knex/repositories/EcommerceRepository';
import { IEcommerceProductsRepository } from '@modules/ecommerce/repository/IEcommerceProductsRepository';
import { EcommerceProductsRepository } from '@modules/ecommerce/infra/knex/repositories/EcommerceProductsRepository';

container.registerSingleton<ICartRepository>('CartRepository', CartRepository);
container.registerSingleton<IEcommerceRepository>(
    'EcommerceRepository',
    EcommerceRepository
);

container.registerSingleton<IEcommerceProductsRepository>(
    'EcommerceProductsRepository',
    EcommerceProductsRepository
);
