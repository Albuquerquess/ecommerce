import { Router, Application } from 'express';

import globalValidator from '../../../../validator/globalValidator';
import EcommerceRoutes from './ecommerce.Routes';

const routes = (app: Application): void => {
    const Routes = Router();
    let routePrefix: string;

    if (process.env.NODE_ENV === 'develop') {
        routePrefix = '/develop';
    } else if (process.env.NODE_ENV === 'homolog') {
        routePrefix = '/homolog';
    } else {
        routePrefix = '/';
    }

    app.use(routePrefix, Routes);

    app.use(globalValidator);
    Routes.use('/ecommerce', EcommerceRoutes);
};

export default routes;
