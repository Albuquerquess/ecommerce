import { AddItemController } from '@modules/ecommerce/useCases/cart/add/addItemController';
import { IndexCartItemsController } from '@modules/ecommerce/useCases/cart/index/indexCartItemsController';
import { RemoveItemController } from '@modules/ecommerce/useCases/cart/remove/removeItemController';
import { UpdateQuantityItemController } from '@modules/ecommerce/useCases/cart/updateQuantity/updateQuantityController';
import { CreateEcommerceController } from '@modules/ecommerce/useCases/ecommerce/create/createEcommerceController';
import { DeleteEcommerceController } from '@modules/ecommerce/useCases/ecommerce/delete/deleteEcommerceController';
import { UpdateEcommerceDNSDataController } from '@modules/ecommerce/useCases/ecommerce/UpdateDNSData/updateEcommerceDNSDatraController';
import { UpdateEcommerceDNSTypeController } from '@modules/ecommerce/useCases/ecommerce/UpdateDNSType/updateEcommerceDNSTypeController';
import { UpdateEcommerceDomainController } from '@modules/ecommerce/useCases/ecommerce/UpdateDomain/updateEcommerceDomainController';
import { UpdateEcommerceTemplateController } from '@modules/ecommerce/useCases/ecommerce/UpdateTemplate/updateEcommerceTemplateController';
import { UpdateEcommerceTitleController } from '@modules/ecommerce/useCases/ecommerce/UpdateTItle/updateEcommerceTitleController';
import { RegisterEcommercePageController } from '@modules/ecommerce/useCases/pages/register/registerEcommercePageController';
import { RegisterProductOptionController } from '@modules/ecommerce/useCases/products/options/registerProductOptionController';
import { IndexProductsByPageController } from '@modules/ecommerce/useCases/products/index/byPageNumber/indexProductsByPageController';
import { RegisterProductReviewController } from '@modules/ecommerce/useCases/products/review/register/registerProductReviewController';

import ecommerceProductsValidator from '@validator/ecommerce/ecommerceProductsValidator';
import ecommerceValidator from '@validator/ecommerce/ecommerceValidator';
import cartValidator from '@validator/ecommerce/cartValidator';
// TODO: Criar rota de criação de categorias
import { Router } from 'express';
import { LinkProductToEcommerceController } from '@modules/ecommerce/useCases/products/link/linkProductToEcommerceController';

const EcommerceRoutes = Router();

const createEcommerceController = new CreateEcommerceController();
const deleteEcommerceController = new DeleteEcommerceController();
const updateEcommerceTitleController = new UpdateEcommerceTitleController();

const addItemController = new AddItemController();
const removeItemController = new RemoveItemController();
const indexCartItemsController = new IndexCartItemsController();
const updateQuantityItemController = new UpdateQuantityItemController();

const registerProductReviewController = new RegisterProductReviewController();
const registerProductOptionController = new RegisterProductOptionController();
const linkProductToEcommerceController = new LinkProductToEcommerceController();
const indexProductsByPageController = new IndexProductsByPageController();

const registerEcommercePageController = new RegisterEcommercePageController();
const updateEcommerceDomainController = new UpdateEcommerceDomainController();
const updateEcommerceDNSTypeController = new UpdateEcommerceDNSTypeController();
const updateEcommerceDNSDataController = new UpdateEcommerceDNSDataController();

const updateEcommerceTemplateController =
    new UpdateEcommerceTemplateController();

// Ecommerce
EcommerceRoutes.post(
    '/create',
    ecommerceValidator.create,
    createEcommerceController.handle
);
EcommerceRoutes.delete(
    // Change status to false
    '/delete',
    ecommerceValidator.delete,
    deleteEcommerceController.handle
);
EcommerceRoutes.patch('/title/update', updateEcommerceTitleController.handle);
EcommerceRoutes.patch(
    '/domain/type/update',
    updateEcommerceDomainController.handle
);
EcommerceRoutes.patch(
    '/dns/type/update',
    updateEcommerceDNSTypeController.handle
);
EcommerceRoutes.patch(
    '/dns/data/update',
    updateEcommerceDNSDataController.handle
);
EcommerceRoutes.patch('/template', updateEcommerceTemplateController.handle);

// Products
EcommerceRoutes.post(
    '/products/review/register',
    registerProductReviewController.handle
);

EcommerceRoutes.post(
    '/products/link/new',
    ecommerceProductsValidator.link,
    linkProductToEcommerceController.handle
);

EcommerceRoutes.get(
    '/products/index',
    ecommerceProductsValidator.indexByPage,
    indexProductsByPageController.handle
);
EcommerceRoutes.post(
    '/products/option/register',
    ecommerceProductsValidator.registeOption,
    registerProductOptionController.handle
);

// Cart
EcommerceRoutes.get(
    '/cart/index',
    cartValidator.index,
    indexCartItemsController.handle
);
EcommerceRoutes.post('/cart/add', cartValidator.add, addItemController.handle);
EcommerceRoutes.patch(
    '/cart/quanitity/update',
    cartValidator.update,
    updateQuantityItemController.handle
);
EcommerceRoutes.delete(
    '/cart/delete',
    cartValidator.delete,
    removeItemController.handle
);

EcommerceRoutes.post('/page/register', registerEcommercePageController.handle);

// EcommerceRoutes.get('/page/index', indexEcommercePageController.handle);
/**
EcommerceRoutes.post(
    '/module/register',
    registerEcommerceModuleController.handle
);
 */
export default EcommerceRoutes;
