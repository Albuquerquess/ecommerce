/* eslint-disable import/prefer-default-export */
import { Knex } from 'knex';

export async function seed(knex: Knex): Promise<void> {
    await knex('products')
        .update({ starter_available: '1' })
        .where({ uid: 1707 });
    await knex('products').update({ starter_available: '1' }).where({ uid: 7 });
}
