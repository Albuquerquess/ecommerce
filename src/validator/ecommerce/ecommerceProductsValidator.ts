import { validate, Segments, Joi } from '@b4-org/middleware-express/Validate';

export default {
    registeOption: validate({
        [Segments.BODY]: Joi.object().keys({
            owner_id: Joi.hashId().required(),
            products: Joi.array().items(
                Joi.object().keys({
                    product_id: Joi.hashId().required(),
                    options: Joi.array().items(
                        Joi.object().keys({
                            title: Joi.string().required(),
                            display_order: Joi.number().required(),
                            values: Joi.array().items(
                                Joi.object().keys({
                                    value: Joi.string().required(),
                                    plan_id: Joi.hashId().required()
                                })
                            )
                        })
                    )
                })
            )
        })
    }),
    link: validate({
        [Segments.BODY]: Joi.object().keys({
            owner_id: Joi.hashId().required(),
            ecommerce_id: Joi.hashId().required(),
            product_id: Joi.hashId().required()
        })
    }),
    indexByPage: validate({
        [Segments.QUERY]: Joi.object().keys({
            ecommerce_id: Joi.hashId().required(),
            per_page: Joi.number()
                .valid(5, 20, 50, 100, 200, 100000)
                .default(20),
            page: Joi.number().default(1)
        })
    })
};
