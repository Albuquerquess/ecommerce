import { validate, Segments, Joi } from '@b4-org/middleware-express/Validate';

export default {
    add: validate({
        [Segments.BODY]: Joi.object().keys({
            ecommerce_id: Joi.hashId().required(),
            plan_id: Joi.hashId().required(),
            client_id: Joi.hashId().required(),
            quantity: Joi.number().default(1).min(1).required()
        })
    }),
    delete: validate({
        [Segments.BODY]: Joi.object().keys({
            ecommerce_id: Joi.hashId().required(),
            plan_id: Joi.hashId().required(),
            client_id: Joi.hashId().required()
        })
    }),
    index: validate({
        [Segments.QUERY]: Joi.object().keys({
            ecommerce_id: Joi.hashId().required(),
            client_id: Joi.hashId().required()
        })
    }),
    update: validate({
        [Segments.BODY]: Joi.object().keys({
            ecommerce_id: Joi.hashId().required(),
            plan_id: Joi.hashId().required(),
            client_id: Joi.hashId().required(),
            new_quantity: Joi.number().min(1).required()
        })
    })
};
