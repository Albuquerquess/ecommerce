import { validate, Joi, Segments } from '@b4-org/middleware-express/Validate';

export default {
    delete: validate({
        [Segments.QUERY]: Joi.object().keys({
            ecommerce_id: Joi.hashId().required(),
            owner_id: Joi.hashId().required()
        })
    }),
    create: validate({
        [Segments.BODY]: Joi.object().keys({
            title: Joi.string().min(1).required(),
            domain: Joi.string().regex(
                /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\\.-]+)+[\w\-\\._~:/?#[\]@!\\$&'(\\)\\*\\+,;=.]+$/
            ),
            owner_id: Joi.hashId().required(),
            DNS: Joi.object().keys({
                type: Joi.string().valid('A', 'AAAA', 'CNAME').required(),
                data: Joi.string()
                    .regex(
                        /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\\.-]+)+[\w\-\\._~:/?#[\]@!\\$&'(\\)\\*\\+,;=.]+$/
                    )
                    .required()
            }),
            template: Joi.object().keys({ path: Joi.string().required() }),
            menus: Joi.array().items(
                Joi.object().keys({
                    label: Joi.string().min(1).trim().required(),
                    submenu: Joi.array().items(
                        Joi.object().keys({
                            label: Joi.string().min(1).trim().required(),
                            url: Joi.alternatives(
                                Joi.string().regex(
                                    /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\\.-]+)+[\w\-\\._~:/?#[\]@!\\$&'(\\)\\*\\+,;=.]+$/
                                ),
                                Joi.string().regex(/\/[a-zA-Z]+|\/[a-zA-Z]+/)
                            ).required()
                        })
                    )
                })
            )
        })
    })
};
